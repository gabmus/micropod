use std::time::Duration;

use gtk::glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

use crate::application::App;
use crate::config::{APP_ID, PROFILE};

mod imp {
    use std::cell::Cell;

    use adw::subclass::prelude::AdwApplicationWindowImpl;

    use crate::player::MicroPodPlayer;

    use super::*;

    #[derive(Debug, gtk::CompositeTemplate)]
    #[template(resource = "/org/gabmus/micropod/ui/window.ui")]
    pub struct AppWin {
        #[template_child]
        pub name_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub artist_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub cover_art: TemplateChild<gtk::Picture>,
        #[template_child]
        pub play_btn: TemplateChild<gtk::Button>,

        pub settings: gio::Settings,
        pub player: MicroPodPlayer,

        pub polling_active: Cell<bool>,
    }

    impl Default for AppWin {
        fn default() -> Self {
            Self {
                name_label: TemplateChild::default(),
                artist_label: TemplateChild::default(),
                cover_art: TemplateChild::default(),
                play_btn: TemplateChild::default(),
                settings: gio::Settings::new(APP_ID),
                player: MicroPodPlayer::new(),
                polling_active: Cell::new(false),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AppWin {
        const NAME: &'static str = "AppWin";
        type Type = super::AppWin;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[gtk::template_callbacks]
    impl AppWin {
        #[template_callback]
        fn on_play_btn_clicked(&self, _btn: &gtk::Button) {
            self.player.play_pause();
            self.instance().set_play_btn_icon();
        }

        #[template_callback]
        fn on_prev_btn_clicked(&self, _btn: &gtk::Button) {
            self.player.skip_prev();
            self.instance().display_metadata(1);
        }

        #[template_callback]
        fn on_next_btn_clicked(&self, _btn: &gtk::Button) {
            self.player.skip_next();
            self.instance().display_metadata(1);
        }
    }

    impl AppWin {
        pub fn start_player_state_polling(&self) {
            self.polling_active.set(true);
            self.start_polling_timeout();
        }

        fn start_polling_timeout(&self) {
            glib::timeout_add_seconds_local(1,
                clone!(@weak self as this => @default-return glib::Continue(false), move || {
                    this.instance().display_metadata(0);
                    return glib::Continue(this.polling_active.get());
                })
            );
        }

        pub fn stop_player_state_polling(&self) {
            self.polling_active.set(false);
        }
    }

    impl ObjectImpl for AppWin {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();

            // Devel Profile
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            // Load latest window state
            obj.load_window_size();

            obj.display_metadata(0);
            self.start_player_state_polling();
        }
    }

    impl WidgetImpl for AppWin {}

    impl WindowImpl for AppWin {
        // Save window state on delete event
        fn close_request(&self) -> gtk::Inhibit {
            if let Err(err) = self.instance().save_window_size() {
                log::warn!("Failed to save window state, {}", &err);
            }

            // Pass close request on to the parent
            self.parent_close_request()
        }
    }

    impl ApplicationWindowImpl for AppWin {}
    impl AdwApplicationWindowImpl for AppWin {}
}

glib::wrapper! {
    pub struct AppWin(ObjectSubclass<imp::AppWin>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl AppWin {

    pub fn new(app: &App) -> Self {
        glib::Object::new(&[("application", app)])
    }

    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let imp = self.imp();

        let (width, height) = self.default_size();

        imp.settings.set_int("window-width", width)?;
        imp.settings.set_int("window-height", height)?;

        imp.settings
            .set_boolean("is-maximized", self.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        let imp = self.imp();

        let width = imp.settings.int("window-width");
        let height = imp.settings.int("window-height");
        let is_maximized = imp.settings.boolean("is-maximized");

        self.set_default_size(width, height);

        if is_maximized {
            self.maximize();
        }
    }

    fn display_metadata(&self, sleep: u32) {
        glib::timeout_add_seconds_local(sleep,
            clone!(@weak self as this => @default-return glib::Continue(false), move || {
                let imp = this.imp();
                imp.name_label.set_text(
                    &imp.player.get_name()
                );
                imp.artist_label.set_text(
                    &imp.player.get_artist()
                );

                let coverart = imp.player.get_cover_art();
                if coverart.is_empty() {
                    imp.cover_art.set_resource(
                        Some("/org/gabmus/micropod/icons/generic_coverart.svg")
                    );
                }
                else {
                    imp.cover_art.set_filename(
                        Some(imp.player.get_cover_art())
                    );
                }
                this.set_play_btn_icon();

                return glib::Continue(false);
            })
        );
    }

    fn set_play_btn_icon(&self) {
        glib::timeout_add_local(Duration::from_millis(100),
            clone!(@weak self as this => @default-return glib::Continue(false), move || {
                this.imp().play_btn.set_icon_name(
                    this.imp().player.get_playback_status_icon()
                );
                return glib::Continue(false);
            })
        );
    }
}
