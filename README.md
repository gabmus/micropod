# MicroPod

MPRIS based standalone media player controller widget.

_This is my first project with Rust and GTK, a demo to get my feet wet with this stack._

[Based on the GTK Rust template](https://gitlab.gnome.org/World/Rust/gtk-rust-template)

![Main window](data/resources/screenshots/screenshot1.png "Main window")

## Building and Running

```bash
./scripts/build.sh && ./scripts/run.sh
```
